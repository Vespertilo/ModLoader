﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harmony;
using UnityEngine;
using System.Collections;
using Steamworks;
using Steamworks.Data;
using VTOLVR.Multiplayer;
using System.Reflection;
using ModLoader.Classes;

namespace ModLoader.Patches
{
    /// <summary>
    /// Adds extra information to the lobby data for modded clients
    /// </summary>
    [HarmonyPatch(typeof(VTMPMainMenu), nameof(VTMPMainMenu.LaunchMPGameForScenario))]
    class VTMPMainMenu_LaunchMPGameForScenario
    {
        private static void Prefix()
        {
            if (!VTOLMPLobbyManager.isLobbyHost)
                return;

            // Sets mods and mods count to check the same mods
            Debug.Log("Setting mod data: " + VTOLAPI.GetUsersOrderedMods());
            VTOLMPLobbyManager.currentLobby.SetData(ModdedMPLobbyKeys.ModLoaderVersion, Assembly.GetAssembly(typeof(ModLoader)).GetName().Version.ToString());
        }

    }

    //Patches the join lobby button to check if the user has the same mods as the lobby
    [HarmonyPatch(typeof(VTMPMainMenu), nameof(VTMPMainMenu.JoinLobby))]
    class VTMPMainMenu_JoinLobby
    {
        private static bool Prefix(VTMPMainMenu __instance, Lobby l)
        {
            Debug.Log("Trying to join modded lobby, checking if this is allowed...");

            string errorMessage = "";

            string hostModloaderVersion = l.GetData(ModdedMPLobbyKeys.ModLoaderVersion);
            string clientModloaderVersion = Assembly.GetAssembly(typeof(ModLoader)).GetName().Version.ToString();

            if (hostModloaderVersion == clientModloaderVersion)
            {
                //modloader versions match, this is fine, do not return early...
            }
            if (hostModloaderVersion == "")
            {
                errorMessage = $"The host's modloader has no version number, tell the host to update their modloader!\n";
                Debug.Log(errorMessage);
                __instance.ShowError(errorMessage);
                return false;
            }
            else if (hostModloaderVersion != clientModloaderVersion)
            {
                errorMessage = $"Modloader version missmatch, hosts is {hostModloaderVersion}, ours is {clientModloaderVersion}\n" +
                    $"Update your modloader version!";
                Debug.Log(errorMessage);
                __instance.ShowError(errorMessage);
                return false;
            }

            Debug.Log("Modloader gives its a-okay to join the server.");
            return true;
        }
    }
}
